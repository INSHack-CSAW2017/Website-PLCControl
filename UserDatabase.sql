use 'plc_manager';

DROP TABLE IF EXISTS `plc_manager`.`users` ;

CREATE TABLE `plc_manager`.`users` (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(50),
    password VARCHAR(255),
    role VARCHAR(20),
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
);

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE `plc_manager`.`users` TO 'web_application';
