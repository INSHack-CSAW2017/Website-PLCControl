<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/'
    ]
];