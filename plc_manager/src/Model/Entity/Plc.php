<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Plc Entity
 *
 * @property int $plc_id
 * @property int $program_id
 * @property int $group_id
 * @property string $identifier
 * @property int $command
 * @property int $status
 * @property int $trust_factor
 * @property bool $corrupted
 * @property bool $active
 *
 * @property \App\Model\Entity\Plc $plc
 * @property \App\Model\Entity\Program $program
 * @property \App\Model\Entity\Group $group
 */
class Plc extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'identifier' => true,
        'command' => true,
        'status' => true,
        'trust_factor' => true,
        'corrupted' => true,
        'active' => true,
        'program' => true,
        'group' => true
    ];
}
