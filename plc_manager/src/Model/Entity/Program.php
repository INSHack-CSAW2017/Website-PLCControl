<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Program Entity
 *
 * @property int $program_id
 * @property int $group_id
 * @property string $name
 * @property string $source
 * @property string|resource $tar_program
 * @property string $build_log
 * @property int $status
 *
 * @property \App\Model\Entity\Program $program
 * @property \App\Model\Entity\Group $group
 */
class Program extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'source' => true,
        'tar_program' => true,
        'build_log' => true,
        'status' => true,
        'group' => true
    ];
}
