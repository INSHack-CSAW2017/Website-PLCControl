<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Plcs Model
 *
 * @property \App\Model\Table\PlcsTable|\Cake\ORM\Association\BelongsTo $Plcs
 * @property \App\Model\Table\ProgramsTable|\Cake\ORM\Association\BelongsTo $Programs
 * @property \App\Model\Table\GroupsTable|\Cake\ORM\Association\BelongsTo $Groups
 *
 * @method \App\Model\Entity\Plc get($primaryKey, $options = [])
 * @method \App\Model\Entity\Plc newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Plc[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Plc|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Plc patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Plc[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Plc findOrCreate($search, callable $callback = null, $options = [])
 */
class PlcsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('plcs');
        $this->setDisplayField('plc_id');
        $this->setPrimaryKey(['plc_id']);

        $this->belongsTo('Programs', [
            'foreignKey' => 'program_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('trust_factor')
            ->notEmpty('trust_factor','')
	    ->add('trust_factor', 'validValue', [
	        'rule' => ['range', 1, 100]
    	    ])
	    ->integer('program_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['program_id'], 'Programs'));
        $rules->add($rules->existsIn(['group_id'], 'Groups'));

        return $rules;
    }
}
