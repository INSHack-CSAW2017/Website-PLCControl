<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * PinMappings Controller
 *
 * @property \App\Model\Table\PinMappingsTable $PinMappings
 *
 * @method \App\Model\Entity\PinMapping[] paginate($object = null, array $settings = [])
 */
class PinMappingsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($group_id = null)
    {
	$group = $this->PinMappings->Groups->get($group_id);

        $pinMappings = $this->paginate($this->PinMappings->find('all')->where(['group_id' => $group->group_id]));

        $this->set(compact('pinMappings','group'));
        $this->set('_serialize', ['pinMappings','group']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($group_id = null)
    {
	$group = $this->PinMappings->Groups->get($group_id);
	if ($group->active === True){
		$this->Flash->error(__('Could not add pin mapping on active group'));
        	return $this->redirect(['action' => 'index',$group->group_id]);
	}

        $pinMapping = $this->PinMappings->newEntity();
        if ($this->request->is('post')) {
            $pinMapping = $this->PinMappings->patchEntity($pinMapping, $this->request->getData());
            $pinMapping->group_id = $group->group_id;
            if ($this->PinMappings->save($pinMapping)) {
                $this->Flash->success(__('The pin mapping has been saved.'));

        	return $this->redirect(['action' => 'index',$group->group_id]);
            }
            $this->Flash->error(__('The pin mapping could not be saved. Please, try again.'));
        }
        $this->set(compact('pinMapping','group'));
        $this->set('_serialize', ['pinMapping','group']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pin Mapping id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

     public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
         $user = $this->Auth->user('id');
    
        if(!$user)
        {        
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        
    }
    public function delete($group_id = null,$id = null)
    {
	$group = $this->PinMappings->Groups->get($group_id);
	if ($group->active===True){
		$this->Flash->error(__('Could not delete pin mapping on active group'));
        	return $this->redirect(['action' => 'index',$group->group_id]);
	}

        $this->request->allowMethod(['post', 'delete']);
        $pinMapping = $this->PinMappings->get($id);
        if ($this->PinMappings->delete($pinMapping)) {
            $this->Flash->success(__('The pin mapping has been deleted.'));
        } else {
            $this->Flash->error(__('The pin mapping could not be deleted. Please, try again.'));
        }

       	return $this->redirect(['action' => 'index',$group->group_id]);
    }
}
