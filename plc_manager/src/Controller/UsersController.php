<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class UsersController extends AppController
{



     public function index()
     {
       $counter = $this->Users->find('list')->count();
       if($counter<=0)
       {
         return $this->redirect(array('controller' => 'users', 'action' => 'add'));
       }
        $this->set('users', $this->Users->find('all'));
        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }

    public function view($id)
    {
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set('user', $user);
    }

     public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $counter = $this->Users->find('list')->count();
         $user = $this->Auth->user('id');
        if($counter>0)
        {
            if(!$user)
            {
                $this->Auth->allow(['logout', 'index']);
            }
            else
            {
                $this->Auth->allow(['logout', 'index','add']);
            }

        }
        else
        {

            $this->Auth->allow(['logout', 'index','add']);


        }
    }

    public function login()
    {
        $counter = $this->Users->find('list')->count();
        if($counter<=0)
        {
          return $this->redirect(array('controller' => 'users', 'action' => 'add'));
        }
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect(array('controller' => 'Groups', 'action' => 'index'));
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}
