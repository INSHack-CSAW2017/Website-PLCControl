<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Alerts Controller
 *
 * @property \App\Model\Table\AlertsTable $Alerts
 *
 * @method \App\Model\Entity\Alert[] paginate($object = null, array $settings = [])
 */
class AlertsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Alerts']
        ];
        $alerts = $this->paginate($this->Alerts);

        $this->set(compact('alerts'));
        $this->set('_serialize', ['alerts']);
    }

    /**
     * View method
     *
     * @param string|null $id Alert id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $alert = $this->Alerts->get($id, [
            'contain' => ['Alerts']
        ]);

        $this->set('alert', $alert);
        $this->set('_serialize', ['alert']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $alert = $this->Alerts->newEntity();
        if ($this->request->is('post')) {
            $alert = $this->Alerts->patchEntity($alert, $this->request->getData());
            if ($this->Alerts->save($alert)) {
                $this->Flash->success(__('The alert has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The alert could not be saved. Please, try again.'));
        }
        $alerts = $this->Alerts->Alerts->find('list', ['limit' => 200]);
        $this->set(compact('alert', 'alerts'));
        $this->set('_serialize', ['alert']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Alert id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $alert = $this->Alerts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $alert = $this->Alerts->patchEntity($alert, $this->request->getData());
            if ($this->Alerts->save($alert)) {
                $this->Flash->success(__('The alert has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The alert could not be saved. Please, try again.'));
        }
        $alerts = $this->Alerts->Alerts->find('list', ['limit' => 200]);
        $this->set(compact('alert', 'alerts'));
        $this->set('_serialize', ['alert']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Alert id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $alert = $this->Alerts->get($id);
        if ($this->Alerts->delete($alert)) {
            $this->Flash->success(__('The alert has been deleted.'));
        } else {
            $this->Flash->error(__('The alert could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['logout']);
    }

}
