<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
/**
 * Programs Controller
 *
 * @property \App\Model\Table\ProgramsTable $Programs
 *
 * @method \App\Model\Entity\Program[] paginate($object = null, array $settings = [])
 */
class ProgramsController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
         $user = $this->Auth->user('id');

        if(!$user)
        {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

    }

   //Check if plcs are using this program
   private function isUsed($program_id) {
 	$Plcs=TableRegistry::get('Plcs');
	$plcs_running = $Plcs->find('all')->where([
		'program_id' => $program_id,
		'status >' => 0
	])->count();

	$plcs_starting = $Plcs->find('all')->where([
		'program_id' => $program_id,
		'command >' => 0
	])->count();
	return ($plcs_running != 0 || $plcs_starting != 0);
   }

   private function getPlcs($program_id) {
 	$Plcs=TableRegistry::get('Plcs');
	return $Plcs->find('all')->where([
		'program_id' => $program_id
	])->all();
   }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($group_id = null)
    {
        $group = $this->Programs->Groups->get($group_id);
        $programs = $this->paginate($this->Programs->find('all')->where(['group_id' => $group->group_id]));

        $this->set(compact('programs','group'));
        $this->set('_serialize', ['programs','group']);
    }

    /**
     * View method
     *
     * @param string|null $id Program id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($group_id=null,$id = null)
    {
        $group = $this->Programs->Groups->get($group_id);
        $program = $this->Programs->get($id);
	$plcs = $this->getPlcs($program->program_id);

        $this->set(compact('program','group','plcs'));
        $this->set('_serialize', ['programs','group','plcs']);
    }

    /**
     * Add method
     *
     * @param string|null $id Group id.
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($group_id = null)
    {
        $program = $this->Programs->newEntity();
	$group = $this->Programs->Groups->get($group_id);

        if ($this->request->is('post')) {
            $program = $this->Programs->patchEntity($program, $this->request->getData());
            $program->group_id = $group->group_id;
            $program->status = 1;
            if ($this->Programs->save($program)) {
                $this->Flash->success(__('The program has been saved.'));

                return $this->redirect(['action' => 'index',$group->group_id]);
            }
            $this->Flash->error(__('The program could not be saved. Please, try again.'));
        }

        $this->set(compact('program', 'group'));
        $this->set('_serialize', ['program']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Program id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($group_id = null, $id = null)
    {
        $program = $this->Programs->get($id);
        $group = $this->Programs->Groups->get($group_id);

	//Check if this program is used by Plcs
	if ($this->isUsed($program->program_id)){
		$this->Flash->error(__('Some PLCs are still running with this program'));
	        return $this->redirect(['action' => 'index',$group->group_id]);
	}

        if ($this->request->is(['patch', 'post', 'put'])) {
            $program = $this->Programs->patchEntity($program, $this->request->getData());
            $program->group_id = $group->group_id;
            $program->status = 1;
            if ($this->Programs->save($program)) {
                $this->Flash->success(__('The program has been saved.'));

                return $this->redirect(['action' => 'index',$group->group_id]);
            }
            $this->Flash->error(__('The program could not be saved. Please, try again.'));
        }
        $this->set(compact('program', 'programs', 'group'));
        $this->set('_serialize', ['program']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Program id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($group_id = null ,$id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $program = $this->Programs->get($id);
        $group = $this->Programs->Groups->get($group_id);


	if ($this->isUsed($program->program_id)) {
		$this->Flash->error(__('Some PLCs are still running with this program'));
	        return $this->redirect(['action' => 'index',$group->group_id]);
	}

        if ($this->Programs->delete($program)) {
            $this->Flash->success(__('The program has been deleted.'));
        } else {
            $this->Flash->error(__('The program could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index',$group->group_id]);
    }
}
