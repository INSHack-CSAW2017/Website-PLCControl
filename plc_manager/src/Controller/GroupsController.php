<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Groups Controller
 *
 * @property \App\Model\Table\GroupsTable $Groups
 *
 * @method \App\Model\Entity\Group[] paginate($object = null, array $settings = [])
 */
class GroupsController extends AppController
{
   //Check if plcs are still running in this group
   private function isUsed($group_id) {
 	$Plcs=TableRegistry::get('Plcs');

	$plcs_running = $Plcs->find('all')->where([
		'group_id' => $group_id,
		'status >' => 0
	])->count();

	$plcs_starting = $Plcs->find('all')->where([
		'group_id' => $group_id,
		'command >' => 0
	])->count();

	return ($plcs_running != 0 || $plcs_starting != 0);
   }

   private function getPlcs($group_id) {
 	$Plcs=TableRegistry::get('Plcs');
	return $Plcs->find('all')->where([
		'group_id' => $group_id
	])->all();
   }

   private function getPrograms($group_id) {
 	$Programs=TableRegistry::get('Programs');
	return $Programs->find('all')->where([
		'group_id' => $group_id
	])->all();
   }
    private function activePlcsCount($group_id) {
 	$Plcs=TableRegistry::get('Plcs');
		return $Plcs->find('all')->where([
			'group_id' => $group_id,
			'activated' => 1
		])->count();
	}

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {


            $user = $this->Auth->user();
            if (!$user)
            {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            else
            {
                $groups = $this->paginate($this->Groups);
	            foreach ( $groups as $group)
                {
		            $group->actives_plcs = $this->activePlcsCount($group->group_id);
	            }
                $this->set(compact('groups'));
                $this->set('_serialize', ['groups']);
            }



    }

    /**
     * Toggle_activation
     *
     * @return \Cake\Http\Response|null Redirects.
     */
    public function toggleActivation($id = null)
    {
        $group = $this->Groups->get($id);
	if (!$group->active && $this->activePlcsCount($group->group_id) === 0){
		$this->Flash->error(__('You need at least one active PLC in this group.'));
	        return $this->redirect(['action' => 'index',$group->group_id]);
	}
        $group->active = !$group->active;
        if ($this->Groups->save($group)) {
            $this->Flash->success(__('Group status has been changed.'));

            return $this->redirect(['action' => 'index']);
        }

    }


    public function add()
    {
        $group = $this->Groups->newEntity();
        if ($this->request->is('post')) {
            $group = $this->Groups->patchEntity($group, $this->request->getData());
            $group->active = False;
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('The group has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The group could not be saved. Please, try again.'));
        }
        $groups = $this->Groups->find('list', ['limit' => 200]);
        $this->set(compact('group', 'groups'));
        $this->set('_serialize', ['group']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Group id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $group = $this->Groups->get($id);
	/*if ($this->isUsed($group->group_id)) {
		$this->Flash->error(__('Some PLCs are still running in this group'));
	        return $this->redirect(['action' => 'index',$group->group_id]);
	}
*/
        if ($this->request->is(['patch', 'post', 'put'])) {
            $group = $this->Groups->patchEntity($group, $this->request->getData());
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('The group has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The group could not be saved. Please, try again.'));
        }
        $this->set(compact('group'));
        $this->set('_serialize', ['group']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Group id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $group = $this->Groups->get($id);
	if ($this->isUsed($group->group_id)) {
		$this->Flash->error(__('Some PLCs are still running in this group'));
	        return $this->redirect(['action' => 'index',$group->group_id]);
	}

        if ($this->Groups->delete($group)) {
            $this->Flash->success(__('The group has been deleted.'));
        } else {
            $this->Flash->error(__('The group could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
