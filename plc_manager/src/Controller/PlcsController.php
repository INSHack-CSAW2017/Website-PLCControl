<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * Plcs Controller
 *
 * @property \App\Model\Table\PlcsTable $Plcs
 *
 * @method \App\Model\Entity\Plc[] paginate($object = null, array $settings = [])
 */
class PlcsController extends AppController
{

     /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
     public function index($group_id = null)
     {
	         //Check if group exists
	          $group = $this->Plcs->Groups->get($group_id);

	           //Get plcs from group
           $plcs = $this->Plcs->find('all',['contain'=>'Programs'])->where(['Plcs.group_id' => $group->group_id]);

	            $paginated_plcs = $this->paginate($plcs);
	             //Send to template
             $this->set(compact('plcs', 'group'));
             $this->set('_serialize', ['plcs']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $user = $this->Auth->user('id');

        if(!$user)
        {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

    }

    public function add($group_id = null)
    {
	//Check if group exists
        $group = $this->Plcs->Programs->Groups->get($group_id);

	//Get the compiled programs from the group
	$programs = $this->Plcs->Programs->find('list')->where([
			'group_id' => $group->group_id,
			'status' => 2
	]);

        $plc = $this->Plcs->newEntity();
        if ($this->request->is('post')) {
	    //Get Data
            $plc = $this->Plcs->patchEntity($plc, $this->request->getData());
            $plc->program_id = $this->request->getData('program_id');
            $plc->group_id = $group->group_id;

            //Set default values for a plc
            $plc->status = 0;
            $plc->command = 0;
            $plc->identifier = '';

	    //Check if the program is valid
	    $program = $this->Plcs->Programs->get($plc->program_id);
	    if ($program->status != 2) {
		$this->Flash->error(__('Program is invalid'));
	    } else {


            	if ($this->Plcs->save($plc)) {
                	$this->Flash->success(__('The plc has been saved.'));

                	return $this->redirect(['action' => 'index', $group->group_id]);
            	}
            	$this->Flash->error(__('The plc could not be saved. Please, try again.'));
	    }
       }

        $this->set(compact('plc', 'programs', 'group'));
        $this->set('_serialize', ['plc']);
    }

    /**
     * Start method
     *
     * @param string|null $id Plc id.
     * @return null Redirects on successful edit.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function start($group_id = null ,$id = null)
    {
	//Only post and delete (CSRF)
	$this->request->allowMethod(['post', 'delete']);

	//Check if group exists
        $group = $this->Plcs->Programs->Groups->get($group_id);

	$plc = $this->Plcs->get($id);

	//Check if plc is stopped
	if ( $plc->status != 0 ){
	    $this->Flash->error(__('The plc should needs to be stopped'));
            return $this->redirect([
		'action' => 'index',
		$group->group_id
	    ]);
	}

	//Check if plc command is already set
	if ( $plc->command != 0 ){
	    $this->Flash->error(__('Already starting...'));
            return $this->redirect([
		'action' => 'index',
		$group->group_id
	    ]);
	}

	//Set command to starting
        $plc->command = 1;

        if ($this->Plcs->save($plc)) {
            $this->Flash->success(__('The plc is starting...'));

            return $this->redirect([
		'action' => 'index',
		$group->group_id
	    ]);
        } else {
            $this->Flash->error(__('The plc could be started. Please try again.'));
        }
    }

    /**
     * Stop method
     *
     * @param string|null $id Plc id.
     * @return null Redirects on successful edit.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function stop($group_id = null ,$id = null)
    {
	//Only post and delete (CSRF)
	$this->request->allowMethod(['post', 'delete']);

	//Check if group exists
        $group = $this->Plcs->Programs->Groups->get($group_id);

	$plc = $this->Plcs->get($id);

	//Check if plc is started or defective
	if ( $plc->status != 1 && $plc->status != 2 ){
	    $this->Flash->error(__('The plc should needs to be started'));
            return $this->redirect([ 'action' => 'index',$group->group_id]);
	}

	//Check if plc command is already set
	if ( $plc->command != 1 ){
	    $this->Flash->error(__('Already stopping...'));
            return $this->redirect([ 'action' => 'index',$group->group_id]);
	}

	//Set command to starting
        $plc->command = 0;
	$plc->activated = False;

	//Patch PLC
        if ($this->Plcs->save($plc)) {
            $this->Flash->success(__('The plc is stopping...'));

        } else {
            $this->Flash->error(__('The plc could be started. Please try again.'));
            return $this->redirect([ 'action' => 'index',$group->group_id]);
        }

	$this->CheckGroupActivation($group);

        return $this->redirect([ 'action' => 'index',$group->group_id]);

    }


    /**
     * View method
     *
     * @param string|null $id Plc id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($group_id = null,$id = null)
    {
	$group = $this->Plcs->Programs->Groups->get($group_id);
        $plc = $this->Plcs->get($id,[
		'contain' => ['Groups','Programs']
	]);

        $this->set('plc', $plc);
        $this->set('group', $group);
        $this->set('_serialize', ['plc','program','group']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Plc id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($group_id = null,$id = null)
    {
        $group = $this->Plcs->Programs->Groups->get($group_id);
        $plc = $this->Plcs->get($id,[
		'contain' => ['Groups','Programs']
	]);

	if ($plc->status != 0 || $plc->command != 0 || $plc->activated === True) {
            $this->Flash->error(__('The plc is busy.'));
            return $this->redirect(['action' => 'index',$group->group_id]);
	}


        $group = $this->Plcs->Groups->get($group_id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $plc = $this->Plcs->patchEntity($plc, $this->request->getData());
            if ($this->Plcs->save($plc)) {
                $this->Flash->success(__('The plc has been saved.'));

                return $this->redirect(['action' => 'index',$group->group_id]);
            }
            $this->Flash->error(__('The plc could not be saved. Please, try again.'));
        }
        $this->set(compact('plc', 'program', 'group'));
        $this->set('_serialize', ['plc']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Plc id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($group_id = null,$id = null)
    {
	//Only post and delete (CSRF)
        $this->request->allowMethod(['post', 'delete']);

        $group = $this->Plcs->Programs->Groups->get($group_id);

        $plc = $this->Plcs->get($id);
	if ($plc->status != 0 || $plc->command != 0 || $plc->activated === True) {
            $this->Flash->error(__('The plc is busy.'));
            return $this->redirect([ 'action' => 'index',$group->group_id]);
	}

        if ($this->Plcs->delete($plc)) {
            $this->Flash->success(__('The plc has been deleted.'));
        } else {
            $this->Flash->error(__('The plc could not be deleted. Please, try again.'));
        }

        return $this->redirect([ 'action' => 'index',$group->group_id]);
    }

    public function toggleActivation($group_id = null,$id = null){
	//Only post and delete (CSRF)
        $this->request->allowMethod(['post', 'delete']);

        $group = $this->Plcs->Programs->Groups->get($group_id);

        $plc = $this->Plcs->get($id);

	if ($plc->activated === True) {
		//Desactivate PLC
		$plc->activated = False;
		$this->Plcs->save($plc);

		$this->CheckGroupActivation($group);

	} else {
		if ($plc->status != 1 || $plc->command != 1){
			$this->Flash->error(__('The plc needs to be started.'));
			return $this->redirect([ 'action' => 'index',$group->group_id]);
		}

		$plc->activated = True;
		$this->Plcs->save($plc);

	}
        return $this->redirect([ 'action' => 'index',$group->group_id]);
    }

	private function CheckGroupActivation($group){
		//If the group is active, desactivate it if it was the last active PLC
		if ($group->active) {
			$nbactives = $this->Plcs->find('all')->where(['activated' => True])->count();
			if ($nbactives === 0) {
				$group->active = False;
				$this->Plcs->Programs->Groups->save($group);
	            		$this->Flash->success(__('Group has been desactivated.'));
			}
		}
	}
}
