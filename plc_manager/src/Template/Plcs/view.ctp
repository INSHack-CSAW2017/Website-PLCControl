<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Plc $plc
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Groups'), ['controller'=>'Groups','action' => 'index']) ?></li>
	<li><?= $this->Html->link(__('Programs'), ['controller'=>'Programs','action' => 'index',$group->group_id]) ?></li>
	<li><?= $this->Html->link(__('Plcs'), ['controller'=>'Plcs','action' => 'index',$group->group_id]) ?></li>
    	<li><?= $this->Html->link(__('Pins'), ['controller'=>'PinMappings','action' => 'index',$group->group_id]) ?></li>
    </ul>
</nav>
<div class="programs index large-9 medium-8 columns content">
    <h3><?= __('Plc') ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Program') ?></th>
            <td><?= $plc->program->name ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Group') ?></th>
            <td><?= $plc->group->name ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Command') ?></th>
            <td><?= array(
                      'Not Created',
                      'Running',
                     )[$this->Number->format($plc->command) == '' ? 0:$plc->command ]
                ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= array(
                      'Not Created',
                      'Running',
                      'Defective',
                     )[$this->Number->format($plc->status) == '' ? 0:$plc->status ]
                ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Trust Factor') ?></th>
            <td><?= $this->Number->format($plc->trust_factor) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Corrupted') ?></th>
            <td><?= $plc->corrupted ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $plc->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
