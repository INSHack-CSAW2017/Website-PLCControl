<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Plc[]|\Cake\Collection\CollectionInterface $plcs
 */
?>

<script type="text/javascript">


 setInterval(function(){ location.reload(); }, 3000);

</script>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Groups'), ['controller'=>'Groups','action' => 'index']) ?></li>
	<li><?= $this->Html->link(__('Programs'), ['controller'=>'Programs','action' => 'index',$group->group_id]) ?></li>
	<li><?= $this->Html->link(__('Plcs'), ['controller'=>'Plcs','action' => 'index',$group->group_id]) ?></li>
    	<li><?= $this->Html->link(__('Pins'), ['controller'=>'PinMappings','action' => 'index',$group->group_id]) ?></li>
    </ul>
</nav>
<div class="programs index large-9 medium-8 columns content">
    <h3><?= "PLCs of group ".$group->name ?></h3>
	<?= $this->Html->link(__('New PLC'), ['action' => 'add', $group->group_id]) ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('program_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('trust_factor') ?></th>
		<th scope="col"><?= $this->Paginator->sort('status') ?></th>
		<th scope="col"><?= $this->Paginator->sort('command') ?></th>
		<th scope="col"><?= $this->Paginator->sort('activated') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($plcs as $plc): ?>
            <tr>
                <td><?= $this->Html->link(__($plc->program->name), ['action' => 'view', $group->group_id,$plc->program->program_id]) ?></td>
                <td><?= $this->Number->format($plc->trust_factor) ?></td>
                <td><?= array(
                      'Not Created',
                      'Running',
                      'Defective',
                     )[$this->Number->format($plc->status) == '' ? 0:$plc->status ]
                ?></td>
                <td><?= array(
                      'Not Created',
                      'Running',
                     )[$this->Number->format($plc->command) == '' ? 0:$plc->command ]
                ?></td>
                <td><?= $this->Form->postLink($this->Number->format($plc->activated)? 'Desactivate':'Activate',['action' => 'toggle_activation', $group->group_id,$plc->plc_id]) ?></td>
                <td class="actions">
                    <?php
		      //We can manage the state of the plc (plcmanager not working)
                      if ($plc->status === $plc->command) {
                          if ($plc->status === 0) {
                            echo $this->Form->postLink(__('Start'),['action' => 'start', $group->group_id,$plc->plc_id]);
                          } else if ($plc->status > 0) {
                            echo $this->Form->postLink(__('Stop'), ['action' => 'stop', $group->group_id,$plc->plc_id]);
                          }
		      }

		      //Plc is defective
                      if ($plc->status === 3) {
		          echo $this->Form->postLink(__('Delete'), ['action' => 'stop', $group->group_id,$plc->plc_id]);
                      }

                      ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $group->group_id,$plc->plc_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $group->group_id, $plc->plc_id], ['confirm' => __('Are you sure you want to delete {0}?', $plc->plc_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
