<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alert[]|\Cake\Collection\CollectionInterface $alerts
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Alert'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="alerts index large-9 medium-8 columns content">
    <h3><?= __('Alerts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('alert_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('level') ?></th>
                <th scope="col"><?= $this->Paginator->sort('message') ?></th>
                <th scope="col"><?= $this->Paginator->sort('source') ?></th>
                <th scope="col"><?= $this->Paginator->sort('read') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($alerts as $alert): ?>
            <tr>
                <td><?= $alert->has('alert') ? $this->Html->link($alert->alert->alert_id, ['controller' => 'Alerts', 'action' => 'view', $alert->alert->alert_id]) : '' ?></td>
                <td><?= $this->Number->format($alert->level) ?></td>
                <td><?= h($alert->message) ?></td>
                <td><?= h($alert->source) ?></td>
                <td><?= h($alert->read) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $alert->alert_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $alert->alert_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $alert->alert_id], ['confirm' => __('Are you sure you want to delete # {0}?', $alert->alert_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
