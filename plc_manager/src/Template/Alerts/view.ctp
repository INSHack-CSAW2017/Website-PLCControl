<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alert $alert
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Alert'), ['action' => 'edit', $alert->alert_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Alert'), ['action' => 'delete', $alert->alert_id], ['confirm' => __('Are you sure you want to delete # {0}?', $alert->alert_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Alerts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Alert'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Alerts'), ['controller' => 'Alerts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Alert'), ['controller' => 'Alerts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="alerts view large-9 medium-8 columns content">
    <h3><?= h($alert->alert_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Alert') ?></th>
            <td><?= $alert->has('alert') ? $this->Html->link($alert->alert->alert_id, ['controller' => 'Alerts', 'action' => 'view', $alert->alert->alert_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Message') ?></th>
            <td><?= h($alert->message) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Source') ?></th>
            <td><?= h($alert->source) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Level') ?></th>
            <td><?= $this->Number->format($alert->level) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Read') ?></th>
            <td><?= $alert->read ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
