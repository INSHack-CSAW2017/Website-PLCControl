<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alert $alert
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Alerts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Alerts'), ['controller' => 'Alerts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Alert'), ['controller' => 'Alerts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="alerts form large-9 medium-8 columns content">
    <?= $this->Form->create($alert) ?>
    <fieldset>
        <legend><?= __('Add Alert') ?></legend>
        <?php
            echo $this->Form->control('level');
            echo $this->Form->control('message');
            echo $this->Form->control('source');
            echo $this->Form->control('read');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
