<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PinMapping $pinMapping
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Groups'), ['controller'=>'Groups','action' => 'index']) ?></li>
	<li><?= $this->Html->link(__('Programs'), ['controller'=>'Programs','action' => 'index',$group->group_id]) ?></li>
	<li><?= $this->Html->link(__('Plcs'), ['controller'=>'Plcs','action' => 'index',$group->group_id]) ?></li>
    	<li><?= $this->Html->link(__('Pins'), ['controller'=>'PinMappings','action' => 'index',$group->group_id]) ?></li>
    </ul>
</nav>
<div class="programs index large-9 medium-8 columns content">
    <?= $this->Form->create($pinMapping) ?>
    <fieldset>
        <legend><?= __('Add Pin Mapping') ?></legend>
        <?php
            echo $this->Form->control('hardware');
            echo $this->Form->control('software');
            echo 'Check for Input'; echo $this->Form->control('direction');
	    
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
