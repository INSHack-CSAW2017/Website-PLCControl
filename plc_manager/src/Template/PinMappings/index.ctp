<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PinMapping[]|\Cake\Collection\CollectionInterface $pinMappings
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Groups'), ['controller'=>'Groups','action' => 'index']) ?></li>
	<li><?= $this->Html->link(__('Programs'), ['controller'=>'Programs','action' => 'index',$group->group_id]) ?></li>
	<li><?= $this->Html->link(__('Plcs'), ['controller'=>'Plcs','action' => 'index',$group->group_id]) ?></li>
    	<li><?= $this->Html->link(__('Pins'), ['controller'=>'PinMappings','action' => 'index',$group->group_id]) ?></li>
    </ul>
</nav>
<div class="programs index large-9 medium-8 columns content">
    <h3><?= __('Pin Mappings') ?></h3>
<?= $this->Html->link(__('New PinMapping'), ['controller'=>'PinMappings','action' => 'add',$group->group_id]) ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('hardware') ?></th>
                <th scope="col"><?= $this->Paginator->sort('software') ?></th>
                <th scope="col"><?= $this->Paginator->sort('direction') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pinMappings as $pinMapping): ?>
            <tr>
                <td><?= $this->Number->format($pinMapping->hardware) ?></td>
                <td><?= $this->Number->format($pinMapping->software) ?></td>
                <td><?= $pinMapping->direction?"Input":"Output" ?></td>
                <td class="actions">
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $group->group_id,$pinMapping->pin_mapping_id], ['confirm' => __('Are you sure you want to delete {0}?', $pinMapping->pin_mapping_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
