<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Program[]|\Cake\Collection\CollectionInterface $programs
 */
?>

<script type="text/javascript">


 setInterval(function(){ location.reload(); }, 3000);

</script>


<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Groups'), ['controller'=>'Groups','action' => 'index']) ?></li>
	<li><?= $this->Html->link(__('Programs'), ['controller'=>'Programs','action' => 'index',$group->group_id]) ?></li>
	<li><?= $this->Html->link(__('Plcs'), ['controller'=>'Plcs','action' => 'index',$group->group_id]) ?></li>
    	<li><?= $this->Html->link(__('Pins'), ['controller'=>'PinMappings','action' => 'index',$group->group_id]) ?></li>
    </ul>
</nav>
<div class="programs index large-9 medium-8 columns content">
    <h3><?= __('Programs') ?></h3>
    <?= $this->Html->link(__('New Program'), ['action' => 'add',$group->group_id]) ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($programs as $program): ?>
            <tr>
                <td><?= $program->name ?></td>
                <td><?= array(
                      'Edition',
                      'Building...',
                      'Builded',
                      'Compilation Error'
                     )[$program->status == '' ? 0:$program->status ]
                ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $group->group_id,$program->program_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $group->group_id,$program->program_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $group->group_id,$program->program_id], ['confirm' => __('Are you sure you want to delete {0}?', $program->name)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
