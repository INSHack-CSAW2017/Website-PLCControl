<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Program $program
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Groups'), ['controller'=>'Groups','action' => 'index']) ?></li>
	<li><?= $this->Html->link(__('Programs'), ['controller'=>'Programs','action' => 'index',$group->group_id]) ?></li>
	<li><?= $this->Html->link(__('Plcs'), ['controller'=>'Plcs','action' => 'index',$group->group_id]) ?></li>
    	<li><?= $this->Html->link(__('Pins'), ['controller'=>'PinMappings','action' => 'index',$group->group_id]) ?></li>
    </ul>
</nav>
<div class="programs index large-9 medium-8 columns content">
    <?= $this->Form->create($program) ?>
    <fieldset>
        <legend><?= __('Add Program') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('source', ['type' => 'textarea', 'escape' => false] );
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
