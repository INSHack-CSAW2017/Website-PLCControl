<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Program $program
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Groups'), ['controller'=>'Groups','action' => 'index']) ?></li>
	<li><?= $this->Html->link(__('Programs'), ['controller'=>'Programs','action' => 'index',$group->group_id]) ?></li>
	<li><?= $this->Html->link(__('Plcs'), ['controller'=>'Plcs','action' => 'index',$group->group_id]) ?></li>
    	<li><?= $this->Html->link(__('Pins'), ['controller'=>'PinMappings','action' => 'index',$group->group_id]) ?></li>
    </ul>
</nav>
<div class="programs index large-9 medium-8 columns content">
    <h3><?= "Program ".$program->name ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= array(
                      'Edition', 
                      'Building...', 
                      'Builded', 
                      'Compilation Error'
                     )[$program->status == '' ? 0:$program->status ] 
                ?></td>
        </tr>
    </table>
    <h3> Plcs Running with this program </h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('trust_factor') ?></th>
		<th scope="col"><?= $this->Paginator->sort('status') ?></th>
		<th scope="col"><?= $this->Paginator->sort('command') ?></th>
		<th scope="col"><?= $this->Paginator->sort('activated') ?></th>
            </tr>
        </thead>
        <tbody>

	<?php foreach ($plcs as $plc): ?>
            <tr>
                <td><?= $this->Number->format($plc->trust_factor) ?></td>
                <td><?= array(
                      'Not Created',
                      'Running',
                      'Defective',
                     )[$this->Number->format($plc->status) == '' ? 0:$plc->status ]
                ?></td>
                <td><?= array(
                      'Not Created',
                      'Running',
                     )[$this->Number->format($plc->command) == '' ? 0:$plc->command ]
                ?></td>
                <td><?= $this->Number->format($plc->activated)? 'Yes':'No' ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <h3 ><?= __('Source') ?></h3>
    <pre><?= h($program->source) ?></pre>

    <h3><?= __('Build Log') ?></h3>
    <pre><?= h($program->build_log) ?></pre>

<?= $this->Html->link(__('Edit'), ['action' => 'edit', $group->group_id,$program->program_id]) ?>
</div>
