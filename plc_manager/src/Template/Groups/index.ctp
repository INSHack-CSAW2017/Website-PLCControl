<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Group[]|\Cake\Collection\CollectionInterface $groups
 */
?>

<div class="groups index large-12 medium-12 columns content" onload="bob();">
    <h3><?= __('List of Plc\'s Groups ') ?></h3>
    <?= $this->Html->link(__('New Group'), ['action' => 'add']) ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('active') ?></th>
                <th scope="col"><?= $this->Paginator->sort('number of active_plcs') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($groups as $group): ?>
            <tr>
                <td><?= h($group->name) ?></td>
                <td><?= $this->Html->link($group->active? __('yes') : __('no'), ['action' => 'toggle_activation', $group->group_id]) ?></td>
                <td><?= $this->Html->link($group->actives_plcs, ['controller' => 'Plcs', 'action' => 'index', $group->group_id ]) ?></td>
		            <td class="actions">
                    <?= $this->Html->link(__('Pins'), ['controller' => 'PinMappings', 'action' => 'index', $group->group_id ]) ?>
                    <?= $this->Html->link(__('Programs'), ['controller' => 'Programs', 'action' => 'index', $group->group_id ]) ?>
                    <?= $this->Html->link(__('Plcs'), ['controller' => 'Plcs', 'action' => 'index', $group->group_id ]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $group->group_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $group->group_id], ['confirm' => __('Are you sure you want to delete group {0}?', $group->name)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<script type="text/javascript">


 setInterval(function(){ location.reload(); }, 3000);

</script>
