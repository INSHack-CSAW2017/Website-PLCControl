<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PlcsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PlcsTable Test Case
 */
class PlcsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PlcsTable
     */
    public $Plcs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.plcs',
        'app.programs',
        'app.groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Plcs') ? [] : ['className' => PlcsTable::class];
        $this->Plcs = TableRegistry::get('Plcs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Plcs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
