<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PinMappingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PinMappingsTable Test Case
 */
class PinMappingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PinMappingsTable
     */
    public $PinMappings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pin_mappings',
        'app.groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PinMappings') ? [] : ['className' => PinMappingsTable::class];
        $this->PinMappings = TableRegistry::get('PinMappings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PinMappings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
