<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PinMappingOutputsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PinMappingOutputsTable Test Case
 */
class PinMappingOutputsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PinMappingOutputsTable
     */
    public $PinMappingOutputs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pin_mapping_outputs',
        'app.groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PinMappingOutputs') ? [] : ['className' => PinMappingOutputsTable::class];
        $this->PinMappingOutputs = TableRegistry::get('PinMappingOutputs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PinMappingOutputs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
