<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PinMappingInputsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PinMappingInputsTable Test Case
 */
class PinMappingInputsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PinMappingInputsTable
     */
    public $PinMappingInputs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pin_mapping_inputs',
        'app.groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PinMappingInputs') ? [] : ['className' => PinMappingInputsTable::class];
        $this->PinMappingInputs = TableRegistry::get('PinMappingInputs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PinMappingInputs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
