#!/bin/bash
sudo apt-get install mariadb-server
sudo apt-get install nginx php-fpm php php-mysql php-mbstring php-intl php-xml
sudo mysql -u root -p < Database.sql
sudo mysql -u root -p < UserDatabase.sql
sudo mkdir /var/log/nginx
sudo chown -R www-data:www-data /var/log/nginx
sudo service nginx start
sudo cat default.conf > /etc/nginx/conf.d/default.conf
sudo cat default > /etc/nginx/sites-enabled/default
sudo cat www.conf > /etc/php/7.0/fpm/pool.d/www.conf
sudo cp -R plc_manager /usr/share/nginx/html/
sudo chown -R www-data:www-data /usr/share/nginx/html/
sudo service php7.0-fpm restart
sudo service nginx restart
